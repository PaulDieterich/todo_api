var express = require("express");
var app = express();
var bodyParse = require("body-parser");


var todoRoutes = require("./routes/todos");

app.use(bodyParse.json());
app.use(bodyParse.urlencoded({extended: true}));
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/public'));

app.get("/", function(req, res){
    res.sendFile("index.html");
})
app.use("/api/todos", todoRoutes);


app.listen(3000, function(){
    console.log("server läuft auf port 3000");
})